use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use chrono::{DateTime, NaiveDateTime, Utc};
use crate::cfg::ProviderConfig;

mod weather_api;
mod accu_weather;

pub const PROVIDERS: [&str; 2] = [
    weather_api::NAME,
    accu_weather::NAME
];

pub trait WeatherProvider {
    fn get_weather(&self, address: &str, day: i32) -> Result<WeatherResponse, WeatherError>;
}

pub struct WeatherResponse {
    date_epoch: i64,
    temp: [f32; 2],
    avg_humidity: Option<f32>,
    precipitation: Option<f32>,
    wind: Option<f32>,
}

impl WeatherResponse {
    pub fn new(date: i64, min_temp: f32, max_temp: f32, humidity: Option<f32>, precipitation: Option<f32>, wind: Option<f32>) -> Self {
        WeatherResponse {
            date_epoch: date,
            temp: [min_temp, max_temp],
            avg_humidity: humidity,
            precipitation,
            wind
        }
    }
}

pub fn get_provider(name: &str, config: ProviderConfig) -> Option<Box<dyn WeatherProvider>> {
    match name {
        weather_api::NAME => Some(Box::new(weather_api::WeatherApiProvider::new(config))),
        accu_weather::NAME => Some(Box::new(accu_weather::AccuWeatherProvider::new(config))),
        _ => None
    }
}

pub enum WeatherError {
    ServiceInaccessible,
    BadResponse,
    NoDay,
    NoAddress
}

impl Debug for WeatherError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            WeatherError::ServiceInaccessible => write!(f, "Service inaccessible"),
            WeatherError::BadResponse => write!(f, "Bad response"),
            WeatherError::NoDay => write!(f, "No day found"),
            WeatherError::NoAddress => write!(f, "No address found")
        }
    }
}

impl Display for WeatherError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            WeatherError::ServiceInaccessible => write!(f, "Service inaccessible"),
            WeatherError::BadResponse => write!(f, "Bad response"),
            WeatherError::NoDay => write!(f, "No day found"),
            WeatherError::NoAddress => write!(f, "No address found")
        }
    }
}

impl Error for WeatherError {

}

impl Display for WeatherResponse {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let date = NaiveDateTime::from_timestamp(self.date_epoch, 0);
        let date: DateTime<Utc> = DateTime::from_utc(date, Utc);
        write!(f, "Date: {}\n", date.format("%d/%m/%Y"))?;
        if self.temp[0] == self.temp[1] {
            write!(f, "\tTemperature: {}C\n", self.temp[0])?;
        } else {
            write!(f, "\tTemperature: {}C - {}C\n", self.temp[0], self.temp[1])?;
        }
        write!(f, "\tHumidity: {}\n", num_no_data(self.avg_humidity, "%"))?;
        write!(f, "\tPrecipitation: {}\n", num_no_data(self.precipitation, "mm"))?;
        write!(f, "\tWind: {}\n", num_no_data(self.wind, "km/h"))
    }
}

fn num_no_data(num: Option<f32>, unit: &str) -> String {
    match num {
        Some(num) => format!("{}{}", num, unit),
        None => String::from("No data")
    }
}