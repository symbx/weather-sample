use serde::Deserialize;

use crate::cfg::ProviderConfig;
use crate::providers::{WeatherError, WeatherProvider, WeatherResponse};

pub const NAME: &str = "accu_weather";

pub struct AccuWeatherProvider {
    config: ProviderConfig
}

impl AccuWeatherProvider {
    pub fn new(config: ProviderConfig) -> Self {
        AccuWeatherProvider {
            config
        }
    }
}

impl WeatherProvider for AccuWeatherProvider {
    fn get_weather(&self, address: &str, day: i32) -> Result<WeatherResponse, WeatherError> {
        let url = format!("http://dataservice.accuweather.com/locations/v1/cities/search?apikey={}&q={}", &self.config.key, address);
        let address = match ureq::get(&url).call() {
            Ok(response) => {
                match response.into_json::<Vec<AddressResponse>>() {
                    Ok(address_obj) => {
                        match address_obj.get(0) {
                            Some(address) => address.key.to_owned(),
                            None => {
                                println!("No address found");
                                return Err(WeatherError::NoAddress)
                            }
                        }
                    },
                    Err(e) => {
                        println!("Fail to decode address: {}", e);
                        return Err(WeatherError::BadResponse);
                    }
                }
            },
            Err(e) => {
                println!("Fail to get address: {}", e);
                return Err(WeatherError::ServiceInaccessible);
            }
        };

        if day == 0 { // Current
            let url = format!("http://dataservice.accuweather.com/currentconditions/v1/{}?apikey={}&details=true", &address, &self.config.key);
            return match ureq::get(&url).call() {
                Ok(response) => {
                    match response.into_json::<Vec<CurrentWeather>>() {
                        Ok(weather) => {
                            match weather.get(0) {
                                Some(weather) => {
                                    Ok(WeatherResponse::new(
                                        weather.epoch_time,
                                        weather.temperature.metric.value,
                                        weather.temperature.metric.value,
                                        Some(weather.relative_humidity),
                                        Some(weather.precipitation_summary.precipitation.metric.value),
                                        Some(weather.wind.speed.metric.value)
                                    ))
                                },
                                None => {
                                    Err(WeatherError::NoDay)
                                }
                            }
                        },
                        Err(e) => {
                            println!("Fail to decode weather: {}", e);
                            Err(WeatherError::BadResponse)
                        }
                    }
                },
                Err(e) => {
                    println!("Fail to get current weather: {}", e);
                    Err(WeatherError::ServiceInaccessible)
                }
            }
        } else { // Forecast
            let url = format!("http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}?apikey={}&details=true&metric=true", &address, &self.config.key);
            // reqwest is awesome, but it's impractical for such small program 
            return match ureq::get(&url).call() {
                Ok(response) => {
                    match response.into_json::<ForecastResponse>() {
                        Ok(weather) => {
                            match weather.daily_forecasts.get((day - 1) as usize) {
                                Some(weather) => {
                                    Ok(WeatherResponse::new(
                                        weather.epoch_date,
                                        weather.temperature.minimum.value,
                                        weather.temperature.maximum.value,
                                        None,
                                        None,
                                        Some((weather.day.wind.speed.value +
                                            weather.night.wind.speed.value) / 2.0)
                                    ))
                                },
                                None => {
                                    Err(WeatherError::NoDay)
                                }
                            }
                        },
                        Err(e) => {
                            println!("Fail to decode weather: {}", e);
                            Err(WeatherError::BadResponse)
                        }
                    }
                },
                Err(e) => {
                    println!("Fail to get current weather: {}", e);
                    Err(WeatherError::ServiceInaccessible)
                }
            }
        }
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct AddressResponse {
    pub key: String
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct CurrentWeather {
    pub epoch_time: i64,
    pub temperature: CurrentTemperature,
    pub relative_humidity: f32,
    pub wind: Wind,
    pub precipitation_summary: PrecipitationSummary
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct CurrentTemperature {
    pub metric: Value
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Wind {
    pub speed: WindSpeed
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct WindSpeed {
    pub metric: Value
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct PrecipitationSummary {
    pub precipitation: Precipitation
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Precipitation {
    pub metric: Value
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Value {
    pub value: f32
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct ForecastResponse {
    pub daily_forecasts: Vec<Forecast>
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Forecast {
    pub epoch_date: i64,
    pub temperature: Temperatures,
    pub day: HalfDay,
    pub night: HalfDay
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Temperatures {
    pub minimum: Value,
    pub maximum: Value
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct HalfDay {
    wind: WindDayData
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct WindDayData {
    pub speed: Value
}
