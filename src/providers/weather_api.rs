use serde::Deserialize;

use crate::cfg::ProviderConfig;
use crate::providers::{WeatherError, WeatherProvider, WeatherResponse};

pub const NAME: &str = "weather_api";

pub struct WeatherApiProvider {
    config: ProviderConfig
}

impl WeatherApiProvider {
    pub fn new(config: ProviderConfig) -> Self {
        WeatherApiProvider {
            config
        }
    }
}

impl WeatherProvider for WeatherApiProvider {
    fn get_weather(&self, address: &str, day: i32) -> Result<WeatherResponse, WeatherError> {
        let url = format!("https://api.weatherapi.com/v1/forecast.json?key={}&q={}&days={}&aqi=no&alerts=no",
                          &self.config.key,
                          address,
            // This service keep at 0 forecast for current day, so +1
                          std::cmp::min(10i32, std::cmp::max(day + 1, 2i32)));
        match ureq::get(&url).call() {
            Ok(response) => {
                match response.into_json::<WeatherApiResponse>() {
                    Ok(res) => {
                        if day == 0 {
                            Ok(WeatherResponse::new(
                            res.current.last_updated_epoch,
                            res.current.temp_c,
                            res.current.temp_c,
                            Some(res.current.humidity),
                            Some(res.current.precip_mm),
                            Some(res.current.wind_kph)
                            ))
                        } else {
                            println!("Got {} days", res.forecast.forecastday.len());
                            match res.forecast.forecastday.get(day as usize) {
                                Some(forecast) => {
                                    Ok(WeatherResponse::new(
                                        forecast.date_epoch,
                                        forecast.day.mintemp_c,
                                        forecast.day.maxtemp_c,
                                        Some(forecast.day.avghumidity),
                                        Some(forecast.day.totalprecip_mm),
                                        Some(forecast.day.maxwind_kph)
                                    ))
                                },
                                None => {
                                    Err(WeatherError::NoDay)
                                }
                            }
                        }
                    },
                    Err(e) => {
                        println!("Decoding error: {}", e);
                        Err(WeatherError::BadResponse)
                    }
                }
            },
            Err(e) => {
                println!("Request error: {}", e);
                Err(WeatherError::ServiceInaccessible)
            }
        }
    }
}

#[derive(Deserialize)]
struct WeatherApiResponse {
    pub current: CurrentWeather,
    pub forecast: ForecastWeather
}

#[derive(Deserialize)]
struct CurrentWeather {
    pub last_updated_epoch: i64,
    pub temp_c: f32,
    pub wind_kph: f32,
    pub humidity: f32,
    pub precip_mm: f32
}

#[derive(Deserialize)]
struct ForecastWeather {
    pub forecastday: Vec<ForecastDayWeather>
}

#[derive(Deserialize)]
struct ForecastDayWeather {
    pub date_epoch: i64,
    pub day: Day
}

#[derive(Deserialize)]
struct Day {
    pub maxtemp_c: f32,
    pub mintemp_c: f32,
    pub avghumidity: f32,
    pub totalprecip_mm: f32,
    pub maxwind_kph: f32
}
