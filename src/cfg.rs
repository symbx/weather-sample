use serde::{Serialize, Deserialize};
use std::fs::File;
use std::collections::HashMap;
use std::io::{BufReader, BufWriter, ErrorKind};
use std::path::PathBuf;
use crate::constants;

// Configuration structure
#[derive(Serialize, Deserialize, Clone)]
pub struct Configuration {
    pub providers: HashMap<String, ProviderConfig>,
    pub default: Option<String>
}

// Provider configuration
#[derive(Serialize, Deserialize, Clone)]
pub struct ProviderConfig {
    pub key: String
}

impl Configuration {
    pub fn new() -> Self {
        Configuration {
            providers: HashMap::new(),
            default: None
        }
    }

    // Load configuration from file or create new
    pub fn load() -> Result<Self, std::io::Error> {
        let path = Configuration::file_path()?;
        match File::open(path) {
            Ok(file) => {
                let reader = BufReader::new(file);
                let cfg = serde_json::from_reader(reader)?;
                Ok(cfg)
            },
            Err(e) => {
                if e.kind() == ErrorKind::NotFound {
                    // If file not exists but has dir - just create empty config
                    Ok(Configuration::new())
                } else {
                    Err(e)
                }
            }
        }
    }

    pub fn save(cfg: Configuration) -> Result<(), std::io::Error> {
        let path = Configuration::file_path()?;
        match File::create(path) {
            Ok(file) => {
                let writer = BufWriter::new(file);
                serde_json::to_writer(writer, &cfg)?;
                Ok(())
            },
            Err(e) => Err(e)
        }
    }

    fn file_path() -> Result<PathBuf, std::io::Error> {
        create_dirs()?; // Just in case
        Ok(dirs::config_dir().unwrap() // Can just unwrap. If no config dir - error drop earlier
            .join(constants::CONFIG_DIR)
            .join(constants::CONFIG_FILE)
            .with_extension("json"))
    }
}

// Create configuration directory if not exists
fn create_dirs() -> Result<(), std::io::Error> {
    match dirs::config_dir() {
        Some(path) => {
            let path = path.join(constants::CONFIG_DIR);
            if !path.is_dir() {
                if path.exists() {
                    std::fs::remove_file(&path)?;
                }
                std::fs::create_dir_all(&path)?;
            }
            Ok(())
        },
        None => {
            Err(std::io::Error::new(ErrorKind::NotFound, "No place for configuration"))
        }
    }
}

impl ProviderConfig {
    pub fn new() -> Self {
        ProviderConfig {
            key: "".to_string()
        }
    }
}