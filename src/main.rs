mod cfg;
mod constants;
mod providers;

fn main() {
    let mut args: Vec<String> = std::env::args().collect();
    args.remove(0); // Remove app name
    match &args.get(0)
        .unwrap_or(&constants::HELP_CMD.to_owned())[..] {
        constants::CONFIGURE_CMD => {
            if args.len() < 2 {
                show_help();
            } else {
                configure_provider(args.get(1).unwrap());
            }
        },
        constants::GET_CMD => {
            if args.len() < 2 {
                show_help();
            } else {
                query_data(args.get(1).unwrap(), args.get(2));
            }
        },
        constants::DEFAULT_CMD => {
            if args.len() < 2 {
                show_help();
            } else {
                set_default(args.get(0).unwrap());
            }
        },
        constants::HELP_CMD => {
            show_help()
        },
        cmd => {
            println!("Unknown command: {}", cmd);
            show_help();
        }
    }
}

fn show_help() {
    println!("You can use this commands: ");
    println!("\t{} <provider> - configure provider", constants::CONFIGURE_CMD);
    println!("\t{} <address> [day] - get weather", constants::GET_CMD);
    println!("\t{} - show help", constants::HELP_CMD);
}

fn configure_provider(provider_name: &String) {
    if !check_providers(provider_name) {
        return;
    }
    match cfg::Configuration::load() {
        Ok(mut config) => {
            // Unpack configuration or create new
            let mut provider = match config.providers.get(provider_name) {
                Some(a) => a.clone(),
                None => cfg::ProviderConfig::new()
            };
            if configure(provider_name, &mut provider) {
                config.providers.insert(provider_name.to_string(), provider);
                if let Err(e) = cfg::Configuration::save(config) {
                    println!("Fail to save configuration: {}", e);
                }
            }
        },
        Err(e) => {
            show_config_error(e);
        }
    }
}

fn show_config_error(e: std::io::Error) {
    println!("Fail to load configuration: {}", e);
}

fn user_input<F>(current: &str, name: &str, validator: Option<F>) -> Option<String>
    where F: Fn(&str) -> bool {
    loop {
        if current.len() > 0 {
            println!("Current value for {}: {}", name, current);
        }
        println!("Enter new value for {} or press enter to leave unchanged:", name);
        let mut value = String::new();
        return match std::io::stdin().read_line(&mut value) {
            Ok(len) => {
                if len == 0 || value.trim().len() == 0 {
                    return Some(current.to_owned());
                }
                if let Some(check) = &validator {
                    if !check(&value) {
                        println!("Incorrect value for {}. Try again.", name);
                        continue;
                    }
                }
                Some(value)
            },
            Err(e) => {
                println!("Error: {}", e);
                None
            }
        }
    }
}

// Dialog to configure provider.
fn configure(name: &str, provider: &mut cfg::ProviderConfig) -> bool {
    println!("Configuring API for {}", name);

    // Example for using validator
    /*let url = match user_input(&provider.url, "URL", Some(|url: &str| {
        let re = regex::Regex::new(r"https?://(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)").unwrap();
        re.is_match(url)
    })) {
        Some(url) => {
            let url = url.trim().to_owned();
            if url.is_empty() {
                println!("You can't have service with no url!");
                return false;
            }
            url
        },
        None => return false
    };*/

    let key = match user_input(&provider.key, "API key", Option::<fn(&str) -> bool>::None) {
        Some(key) => key,
        None => return false
    };
    // provider.url = url;
    provider.key = key;
    println!("Done!");
    true
}

fn check_providers(name: &str) -> bool {
    if !providers::PROVIDERS.contains(&name) {
        println!("This application contains implementation only for: {}", providers::PROVIDERS.join(", "));
        return false;
    }
    true
}

fn query_data(address: &String, day: Option<&String>) {
    match cfg::Configuration::load() {
        Ok(config) => {
            if config.providers.is_empty() {
                println!("You have no providers. Configure at least one to get data.");
                return;
            }
            if config.default.is_none() {
                println!("Choose default provider first please.");
                return;
            }
            let provider_name = config.default.unwrap();
            if !check_providers(&provider_name) {
                return;
            }
            if let Some(provider_config) = config.providers.get(&provider_name) {
                if let Some(provider) = providers::get_provider(&provider_name, provider_config.clone()) {
                    let day = match day {
                        Some(day) => day.to_owned(),
                        None => constants::NOW.to_owned()
                    };
                    let day = match &day[..] {
                        constants::NOW => 0,
                        constants::TOMORROW_1 | constants::TOMORROW_2 => 1,
                        day => {
                            match day.parse::<i32>() {
                                Ok(e) => e,
                                Err(_) => {
                                    println!("Fail to parse day");
                                    return;
                                }
                            }
                        }
                    };
                    match provider.get_weather(address, day) {
                        Ok(response) => {
                            println!("{}", response);
                        },
                        Err(e) => {
                            println!("Fail to get weather data: {}", e);
                        }
                    }
                }
            } else {
                println!("Yoy have no configured {} provider", &provider_name);
            }
        },
        Err(e) => {
            show_config_error(e);
        }
    }
}

fn set_default(provider: &String) {
    if !check_providers(provider) {
        return;
    }
    match cfg::Configuration::load() {
        Ok(mut config) => {
            config.default = Some(provider.to_owned());
            if let Err(e) = cfg::Configuration::save(config) {
                println!("Fail to save configuration: {}", e);
            }
        },
        Err(e) => {
            show_config_error(e);
        }
    }
}