// Commands
pub const CONFIGURE_CMD: &str = "configure";
pub const GET_CMD: &str = "get";
pub const DEFAULT_CMD: &str = "default";
pub const HELP_CMD: &str = "help";

// Path constants
pub const CONFIG_DIR: &str = "weather-sample";
pub const CONFIG_FILE: &str = "config";

// Days shorthand
pub const NOW: &str = "now";
pub const TOMORROW_1: &str = "tomorrow";
pub const TOMORROW_2: &str = "next";